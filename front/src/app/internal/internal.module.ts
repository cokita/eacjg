import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LayoutComponent} from './layout/layout.component';
import {HomeComponent} from './home/home.component';
import {InternalRoutingModule} from './internal-routing.module';
import {MaterialModule} from '../shared/material/material.module';
import {SharedModule} from '../shared/shared.module';

@NgModule({
    declarations: [LayoutComponent, HomeComponent],
    imports: [
        MaterialModule,
        SharedModule,
        CommonModule,
        InternalRoutingModule
    ]
})
export class InternalModule {
}
