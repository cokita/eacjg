<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
         $this->call(ProfilesTableSeeder::class);
         $this->call(UsersProfilesTableSeeder::class);
         $this->call(OAuthSeeder::class);
         $this->call(FilesTypeTableSeeder::class);
//         $this->call(ActionsTableSeeder::class);
//         $this->call(ProfilesActionsTableSeeder::class);
    }
}
