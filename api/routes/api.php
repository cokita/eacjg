<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
Route::middleware('jwt.refresh')->get('/token/refresh', 'AuthController@refresh');
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::post('signup', ['as' => 'user.store', 'uses' => 'UserController@store']);
//Route::post('login', ['as' => 'user.login', 'uses' => 'AuthController@login']);


//Resources e rotas exclusivos para ADM
Route::group(['middleware' => ['auth:api', 'roles'],'roles' => ['administrador']], function () {
    Route::group(['prefix' => 'user'], function () {
        Route::delete('/{id}', ['as' => 'user.destroy', 'uses' => 'UserController@destroy']);
    });

    Route::get('auth/logout', ['as' => 'user.logout', 'uses' => 'AuthController@logout']);

    Route::resource('file', 'FileController', ['only' => ['show', 'index']]);

    Route::group(['prefix' => 'user'], function () {
        Route::get('/get-identity', ['as' => 'user.identity', 'uses' => 'UserController@getUserForLogin']);
        Route::get('/{id}', ['as' => 'user.show', 'uses' => 'UserController@index']);
        Route::put('/{id}', ['as' => 'user.update', 'uses' => 'UserController@update']);
        Route::get('/', ['as' => 'user.index', 'uses' => 'UserController@index']);
        Route::post('/', ['as' => 'user.store', 'uses' => 'UserController@store']);
    });

    Route::group(['prefix' => 'file'], function () {
        Route::post('/{id}', ['as' => 'show', 'uses' => 'FileController@show']);
        Route::post('/', ['as' => 'store', 'uses' => 'FileController@store']);
    });

});

//Resources e rotas exclusivos para ADM e VENDEDORES
//Route::group(['middleware' => ['auth:api', 'roles'],'roles' => ['administrador', 'vendedor']], function () {
//
//
//});





