<?php

namespace App\Constants;

class ConstProfile {

    CONST ADMIN = 1;
    CONST USUARIO = 2;

    CONST ARR_ALL_PROFILES = [self::ADMIN,self::USUARIO];
}