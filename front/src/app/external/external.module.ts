import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import { LayoutComponent } from './layout/layout.component';
import { HomeComponent } from './home/home.component';
import {ExternalRoutingModule} from './external-routing.module';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { AboutusComponent } from './aboutus/aboutus.component';

@NgModule({
  declarations: [LayoutComponent, HomeComponent, HeaderComponent, FooterComponent, AboutusComponent],
  imports: [
    ExternalRoutingModule,
    CommonModule,
    SharedModule
  ]
})
export class ExternalModule { }
