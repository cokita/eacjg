import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import {MaterialModule} from './material/material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {FilterPipeModule} from 'ngx-filter-pipe';
import {NgxSpinnerModule} from 'ngx-spinner';
import { SendEmailComponent } from './modal/send-email/send-email.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    declarations: [SendEmailComponent],
    imports: [
        CommonModule,
        MaterialModule,
        FilterPipeModule,
        NgxSpinnerModule,
        FontAwesomeModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule
    ],
    exports: [
        CommonModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        FilterPipeModule,
        NgxSpinnerModule,
        SendEmailComponent,
        NgbModule,
        FontAwesomeModule
    ]
})
export class SharedModule {
    constructor() {
        // Add an icon to the library for convenient access in other components
        library.add(fas, far, fab);
    }
}